﻿**README**
Written by Alexander Hausmann (alexander_hausmann@gmx.net) in ~November 2017


**Installation and workflow:**


The content of this folder contains all files that are necessary to analyze, pre- and postprocess video recordings 
of butterfly behaviour, except for the two C++ .exe programs. One of these programs (depending on user's choice) 
is called by the main batch script of this folder and has to be generated by the user on his/her own machine, 
having given the C++ code which can be found in another repository. For creating this CheckMotion.exe 
and the MovenentIntoFrame programs, please check out the README in the OpenCV repository.

For detailed information, read comments in each script/program!

First of all: The code in this folder was written for a Windows machine. It was written for videos shot with a GoPro
camera, which has the problem that it a) can only shoot with sound on and b) saves the files of one recording session
as chunks. This is one big aspect that the here presented code has to deal with, as e.g. OpenCV breaks down with GoPro
sound in videos.

To make the program run, you need to have ffmpeg and R installed on your machine.
I downloaded *ffmpeg-20171111-3af2bf0-win64-static.zip* from https://ffmpeg.zeranoe.com/builds/win64/static/
Unpack with *7zip*.
Follow the installation guide in the following link, but it is easier not to do step 3, unless you want to use 
ffmpeg in the future independently of this program:
http://adaptivesamples.com/how-to-install-ffmpeg-on-windows/
Make sure you have R installed. Follow any common guide. 

Create a main folder for your analyses. Inside this folder, create a folder called get_timestamps. If you call it 
differently, the program will change the name to get_timestamps automatically.
Copy all files provided in this repository into that get_timestamps folder. Furthermore, built the two C++/OpenCV
programs provided in the OpenCV repository in your IDS and copy the executables into the get_timestamps folder
(or only build one of the two programs, if you only want to use one version of motion detection).
ATTENTION: these executables have to have the following names: "MovementIntoFrame.exe" and "CheckMotion.exe".
Read the installation guide in the OpenCV repository on how to build them.

Inside get_timestamps, double-click the "SETUP" file. Choose which method of detection to use, set parameters for the
program, provide paths to ffmpeg and R and give your final results sheet a name.

In your main folder (the parent folder of get_timestamps), a results sheet and a batch file "GET_TIMESTAMPS" will appear.

Inside the main folder, you will create subfolders named e.g. after the date, whereas it is best to name them in a year_month_day style and with months as numbers
as the computer will then order them descendingly.
If you decided in the setup to have input and output location inside the same folders, you need to create these result folders inside your
date folders, and copy the video material into them. At least one new subfolder in this folder is necessary! It can though be as many subfolders as you want.
For example, if on the the 19th of December, you shot video footage of two different species, create the folder 17_12_19 and inside, create a subfolder for the footage of one 
and another subfolder for the footage of the other species. No more subfolders inside the subfolders! And don't change the video file names!
The program will run independently over them. If you decided to extract videos from external folder (e.g. another location on your computer or
a SD card, you don't need to create any subfolders inside the date folder, as the program will do so automatically. 

Now you can move back to the main folder and double-click on GET_TIMESTAMPS.bat. The program will ask you for the
name of the folder where you just stored your videos, i.e. e.g. the date under which you saved them. Hit enter and the
program will do the rest.

The program will now automatically clip all videos of the same recording session together and strip the sound off from
all files. The resulting video files will be called 1.mp4, 2.mp4 and so on. One such file represents one continuous
recording. It will retrieve video information on all videos. It will feed the videos into the selected OpenCV program
and will transform the output of this program into well readable video-timecode which represents the beginning of each movement. 
After that, it will paste a blank csv sheet into your folder which you will have to fill with the data of this 
day. Finally, the batch scripts relevant for the next steps are copied into each of those subfolders.

If you are not happy with the results from the OpenCV program, you can run the code again separetly over a video
by clicking rerun_opencv_code.bat. You can select new parameter settings. The output will only contain information
on this one video.

In each subfolder, open now the .csv file that was created, open *RELEVANT_timestamps.txt* and go through each video 
(1, 2, 3, 4) and fill in the data into the .csv file. Enter the beginning time either as seconds since beginning of 
video or as a hh.mm.ss timecode (use dots between the units!! It is ok if your write 8 for 8am and not 08). 

Click on *000_submit_results.bat* then to transform and to append the combined data on video information and your results
automatically to the results csv file in the main folder.

Finally, double-click 000_cut_video.bat to delete your big video files and paste together the relevant chunks,
given the information in your results file into a supercut. Attention: Those videos will be gone forever. So be sure you read all information out of them. 
Execute this only if you already hit *submit_results.bat*.


**Files:**

- ***results_blueprint.csv:***
This is the final table in which information from all videos will be collected. This file never has to be opened, 
as the results of a certain day simply get appended to it with a batch script. It will be copied from get_timestamps
in your parent folder under your chosen name.

- ***copy_main_program.bat:*** 
Transforms videos into a readable format for the C++ code *MovementIntoFrame.exe* or *CheckMotion.exe* 
which give information on the frames which contain an alive butterfly. The C++ output is then modified into a more 
readable format by the R script ***Transform_raw_code.R***. Moreover, this batch script collects information about the 
different videos (recording time and duration), which is then processed in the R scripts ***Extract_video_info1.R*** 
and ***Extract_video_info1_duration.R***. Will be extracted to your parent directory as GET_TIMESTAMPS.bat.

- ***copy_submit_results_program.bat:***
This batch script merges and transforms the video information and the data that was collected and appends them to 
the main *results.csv* table. The R script ***data_submission.R*** is used for the purpose of merging and 
transforming. Will be extracted to each subfolder which contains video material as 000_submit_results.bat.

- ***copy_cut_video_program.bat:***
This batch script takes information from the results table and cuts out chunks from the videos in which butterflies 
were observed. Then, those chunks get merged together again and the full-time videos deleted. Fot that purpose, the 
R script ***video_cutter.R*** is called. Will be extracted to each subfolder which contains video material as 000_submit_results.bat.

- ***copy_rerun_opencv_code.bat***
This batch script is a short version of GET_TIMESTAMPS.bat which can be called after GET_TIMESTAMPS.bat if you are
not satisfied with the output for a specific video file. This batch script calls the R script 
Transform_raw_code_rerun_opencv.R. This batch script will be extracted to each subfolder which contains video material as 000_rerun_opencv.bat.




Appendix:

**Analyze a video:**
Download *VLC player* and use *CTRL+T* to jump to a certain second and pause video a few frames before object enters 
the frame and then use *E* to count number of frames with object inside. 
Go: *Tools >> Preferences >> (Select "All" for "Show settings") >> Interface >> Hotkeys settings* 
to change the very short jump length to 1 sec and the short one to 3 sec. Use hotkeys for those: *Shift+left* and 
*Alt+left*.
Check hotkeys here: https://www.howtogeek.com/196371/master-vlc-with-these-23-keyboard-shortcuts/


To read more about stripping sound off: http://www.bugcodemaster.com/article/transforming-video-file-remove-sound 
and about concatenation: https://trac.ffmpeg.org/wiki/Concatenate
