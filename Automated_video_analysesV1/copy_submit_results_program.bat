@echo off
REM Let Windows not echo anything unless it is asked to do so

REM Written by Alexander Hausmann (alexander_hausmann@gmx.net) in ~ November 2017 for a Windows machine

REM This batch file can be executed either by double-clicking on the file name or calling it from the console.

REM This batch script executes the following tasks (by itself or by calling other programs):
REM a) Combine the results csv file and the video information file
REM b) Append this to the main results csv in the mother folder
REM c) Extract timestamps for later cutting of the video


REM Remember current directory
set current_dir=%cd%

for %%F in ("%cd%") do set "only_folder_species=%%~nxF"

REM Go down two levels. Safe folder name in between.
cd..
for %%F in ("%cd%") do set "only_folder_date=%%~nxF"
cd..

REM Update main directory file
cd > get_timestamps\directories\main_dir.txt

REM Read in directories and save them on variables.
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\main_dir.txt`) DO (
SET main_dir=%%F
)
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\R_dir.txt`) DO (
SET R_dir=%%F
)
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\results_name.txt`) DO (
SET results_name=%%F
)

REM Go back to subfolder
cd %current_dir%

REM Replace \ in directory by / for R.
set current_dir=%current_dir:\=/%

REM Call the R script that will merge and transform information from the result csv file and the video information file and safe it as a new csv file.
"%R_dir%" CMD BATCH --vanilla --slave "--args %current_dir%" %main_dir%\get_timestamps\data_submission.R

REM Append information from this new csv file to the main results csv in the mother folder.
REM copy /b %main_dir%\%results_name%.csv + results_submit.csv %main_dir%\%results_name%.csv

REM Delete all files that are not needed anymore or are just unnecessary leftovers.
del %main_dir%\get_timestamps\data_submission.Rout
REM Also delete the rerun code for openCV as the user apparently decided that the runs were fine.
del 000_rerun_opencv_code.bat
REM del 000_submit_results.bat

REM echo %only_folder_date% >> %main_dir%\done_%only_folder_species%.txt

REM END