@echo off
REM Let Windows not echo anything unless it is asked to do so

REM Written by Alexander Hausmann (alexander_hausmann@gmx.net) in ~ November 2017 for a Windows machine

REM This batch file can be executed either by double-clicking on the file name or calling it from the console.

REM This batch script executes the following tasks (by itself or by calling other programs):
REM a) Cut out chunks from the videos according to the moments in the video where a butterfly is supposed to be in.
REM b) Clip these videos together.
REM c) Delete the old videos that cover the full filming period.


REM This is a safety step: If results were not yet submitted to the main results file, submit_results.bat should still exist in the directory. 
REM Thus the program will be automatically closed.
IF EXIST "submit_results.bat" EXIT [/B]
IF NOT EXIST "results_submit.csv" EXIT [/B]

REM Remember current directory
set current_dir=%cd%

REM Go down two levels
cd..
cd..

REM Update main directory file
cd > get_timestamps\directories\main_dir.txt

REM Read in directories and save them on variables.
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\main_dir.txt`) DO (
SET main_dir=%%F
)
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\R_dir.txt`) DO (
SET R_dir=%%F
)
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\ffmpeg_dir.txt`) DO (
SET ffmpeg_dir=%%F
)

REM Go back to subfolder
cd %current_dir%

REM Replace \ in directory by / for R.
set current_dir=%current_dir:\=/%

REM Replace \ in ffmpeg directory by \\ for R.
set ffmpeg_dir=%ffmpeg_dir:\=\\%

REM Give all mp4 files the extension "_old". This is necessary to later delete them. I think the "dir /b /a-d" is actually not really necessary here...
for /f "delims=" %%i in ('dir /b /a-d *.mp4') do ren "%%~i" "%%~ni%%~xi_old"

REM Call R to cut the video and merge the chunks together, if they came from the same video.
REM Inside R, the command line will be called to ease the whole batch looping. Check the R scripts for details.
"%R_dir%" CMD BATCH --vanilla --slave "--args %current_dir% %ffmpeg_dir%" %main_dir%\get_timestamps\video_cutter.R


REM This part is not so elegant. Since ffmpeg seems to be incapable of cutting precise chunks with the copy function
REM (see http://trac.ffmpeg.org/wiki/Seeking), I work around that with determining the length of the cut chunk to find out
REM where in the supercut a behavour is located. Some parts of R scripts (e.g. results_submit.R) have some unnecessary code
REM which calculates the position in the supercut in a wrong way. Since this calculation does not take so long,
REM I didn't change the whole code to not do what it is doing. Instead, the the column with the old calculation just gets 
REM replaced by the function output in video_cutter_chunk_duration.R.


REM Get info on chunks.
for %%a in (chunk*.mp4) do %ffmpeg_dir% -i %%~a > %%~nxa_info 2>&1

REM Extract duration
for %%i in (*info) do findstr /R [a-z]*Duration  %%i > forR_%%i.txt

REM Call R to calculate position in supercut from duration of chunks.
"%R_dir%" CMD BATCH --vanilla --slave "--args %current_dir% %ffmpeg_dir%" %main_dir%\get_timestamps\video_cutter_chunk_duration.R


REM Delete full duration video files.
REM del *_old

REM Delete all files that are not needed anymore or are just unnecessary leftovers.
del %main_dir%\get_timestamps\video_cutter.Rout
del %main_dir%\get_timestamps\video_cutter_chunk_duration.Rout
del *chunks.txt
del timestamps.txt
del chunk_*
del which_chunk.txt
del forR*
del names_chunks.txt
del 000_cut_video.bat




REM END