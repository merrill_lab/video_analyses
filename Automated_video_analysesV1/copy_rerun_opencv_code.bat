@ECHO OFF
REM Let Windows not echo anything unless it is asked to do so

REM Written by Alexander Hausmann (alexander_hausmann@gmx.net) in ~ November 2017 for a Windows machine

REM This batch file can be executed either by double-clicking on the file name or calling it from the console.

REM This script executes parts of the main batch script. It can be called by the user if timestamps for a specific video should be created again with new parameter settings in the openCV code.


REM Ask user if he wants to run program under same parameter settings.
set /p new_params="Do you want to change any parameter settings of the program or just re-run it under same settings? Type y for new parameters or n for old settings :"

REM Ask user for filename to reanalyze
set /p filename="Type in name of mp4 file to re-analyze, e.g. 1.mp4: "

REM Set current directory
set current_dir=%cd%

REM Go into main directory
cd..
cd..

REM Read in directories and save them on variables.
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\main_dir.txt`) DO (
SET main_dir=%%F
)
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\R_dir.txt`) DO (
SET R_dir=%%F
)

REM Read in the decision the user has made (motion or color detection) and read in user set parameters
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\motion_or_color.txt`) DO (
SET motion_or_color=%%F
)

REM If user decided to use old settings, read them in.
IF %new_params%==n (
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\exe_program_parameters.txt`) DO (
SET program_parameters=%%F
)
)

cd %current_dir%

REM If user decided to use new settings, ask for them.
IF %new_params%==y (
IF %motion_or_color%==1 (
REM Ask the user for the sensitivity for motion and for intensity of blurring.
set /p sensitivity="Set the sensitivity of the program (e.g. 80): "
set /p blur="Set the intensity of blur to reduce noise (e.g. 10): "
REM Ask the user for the size of the ignored area.
set /p bottom="This and the following parameters specify the area of interest in the videos by defining a rectangular shaped area in the frame that gets ignored. The region of interest is thus not an included are, but it excludes an area inside of it. The setting for this and the following 3 parameters has to lie between 0 and 1. The sum of the proportion of left and right as well as the sum of the proportion of bottom and top cannot exceed 1. If both sums are 1, no area in the video will be ignored. If both sums are 0, the whole video will be ignored. For this current parameter, select the area on the bottom of the video that is of interest, e.g. a proportion 0.25 of the y-dimension pixels is of interest, looking from the lower side: "
set /p left="Set the proportion of pixels on left edge that are of interest, e.g. 0.25: "
set /p top="Set the proportion of pixels on top edge that are of interest, e.g. 0.25: "
set /p right="Set the proportion of pixels on right edge that are of interest, e.g. 0.25: "
set /p plot_RONI="Now decide whether for each video that gets processed by the program, if a visualization of the area of no interest should be stored in the folder in which the respective video is stored. Type y for yes and n for n: "
)
IF %motion_or_color%==1 (
REM Safe on one variable
set program_parameters=%sensitivity% %blur% %bottom% %left% %top% %right% %plot_RONI%
)

IF %motion_or_color%==2 (
REM Ask the user for the BGR minimums and maximums
set /p B_min="Specify the lowest value for Blue in RGB code: "
set /p G_min="Specify the lowest value for Green in RGB code: "
set /p R_min="Specify the lowest value for Red in RGB code: "
set /p B_max="Specify the highest value for Blue in RGB code: "
set /p G_max="Specify the highest value for Green in RGB code: "
set /p R_max="Specify the highest value for Red in RGB code: "
)
IF %motion_or_color%==2 (
REM Safe on one variable
set program_parameters=%B_min% %G_min% %R_min% %B_max% %G_max% %R_max%
)
)


REM As I have a bit of a problem understanding IF clauses in batch, I circumvent this by executing first the color detection program (if selected)
REM and all subsequent steps.

REM Call the C++/OpenCV program to output a table which contains all frames that match a certain given condition (in this case it finds those frames in
REM which a signal color can be detected). Do this for each video in the folder. The last entry in the created table is the frame rate.
if %motion_or_color%==2 (
%main_dir%\get_timestamps\CheckMotion.exe %current_dir%\ %filename% %program_parameters% > raw_%filename%_new.txt
)
if %motion_or_color%==2 (
REM Replace \ in directory by / for R.
set current_dir=%current_dir:\=/%
)

if %motion_or_color%==2 (
REM Call R to modify this output in a way that it condenses the information that the C++ program gives down to something more convenient for the user.
REM Check comments in the R script for more.
"%R_dir%" CMD BATCH --vanilla --slave "--args %current_dir% %filename%" %main_dir%\get_timestamps\Transform_raw_code_rerun_opencv.R
)
if %motion_or_color%==2 (
REM Delete Rout file.
del %main_dir%\get_timestamps\Transform_raw_code_rerun_opencv.Rout
)



IF %motion_or_color%==1 (
IF %plot_RONI%==y (
IF EXIST "%filename%_frame1_excluded_area.png" (
IF NOT EXIST %filename%_frame1_excluded_area_OLD.png (
rename %filename%_frame1_excluded_area.png %filename%_frame1_excluded_area_OLD.png
)
)
)
)

REM Call the C++/OpenCV program to output a table which contains all frames that match a certain given condition (in this case it finds those frames in
REM which motion into frame can be detected). Do this for each video in the folder. The last entry in the created table is the frame rate.
if %motion_or_color%==1 (
%main_dir%\get_timestamps\MovementIntoFrame.exe %current_dir%\ %filename% %program_parameters% > raw_%filename%_new.txt
)
REM Replace \ in directory by / for R.
set current_dir=%current_dir:\=/%

REM Call R to modify this output in a way that it condenses the information that the C++ program gives down to something more convenient for the user.
REM Check comments in the R script for more.
"%R_dir%" CMD BATCH --vanilla --slave "--args %current_dir% %filename%" %main_dir%\get_timestamps\Transform_raw_code_rerun_opencv.R

REM Delete Rout file.
del %main_dir%\get_timestamps\Transform_raw_code_rerun_opencv.Rout


echo %program_parameters% > param_rerun_%filename%.txt



REM END