@ECHO OFF
REM Let Windows not echo anything unless it is asked to do so

REM Written by Alexander Hausmann (alexander_hausmann@gmx.net) in ~ November 2017 for a Windows machine

REM This batch file can be executed either by double-clicking on the file name or calling it from the console.

REM To make this batch script and all other scripts and programs called by it work, you have to click and follow the SETUP.bat according
REM to your the settings on your system.
REM Videos are split up by dates of recording and in each such folder, videos are split into subfolders.
REM This program can only analyze one such folder, if clicked. If you want to run it over multiple folders, you have to write a batch file that does this.
REM If you run the program with raw video files being stored at the same location where the output will be produced in, then those videos need to
REM be ordered in subfolders by video class, e.g. inside 17_12_18 are two subfolders, species1 and species 2 with video data on both of them.
REM Even if there is only one set of videos, it still needs to go into a subfolder.
REM If data is being extracted from external location, program will create those subfolders in case they don't exist yet.
REM Attention: Extraction location as well as output location can never contain any other .mp4 files that are of no current interest.
REM This program will use ALL available videos. 

REM As typical for GoPRO, videos will be stored by the camera as split into small chunks which all have the same identifier at the end of the filename. 
REM The goal: Get a video file in which all chunks of one recording are clipped together and a table which tells the user at which timepoints of the video
REM (in seconds) to expect objects moving in frame.

REM This batch script executes the following tasks (by itself or by calling other programs):
REM a) It removes audio from our GoPRO video files (this is necessary for the code to work)
REM b) It clips those videos together that come from the same recording
REM c) It outputs a table with the relevant time points at which butterflies are to be expected.


REM This allows variables to change over the course of the script. Further on, variables have to be called with an exclamation mark in the beginning and at the end
setlocal enabledelayedexpansion

REM Update main directory file
cd > get_timestamps\directories\main_dir.txt

REM Read in directories and save them on variables.
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\main_dir.txt`) DO (
SET main_dir=%%F
)
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\ffmpeg_dir.txt`) DO (
SET ffmpeg_dir=%%F
)
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\R_dir.txt`) DO (
SET R_dir=%%F
)

REM Read in the decision the user has made (motion or color detection) and read in user set parameters
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\motion_or_color.txt`) DO (
SET motion_or_color=%%F
)
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\exe_program_parameters.txt`) DO (
SET program_parameters=%%F
)

REM Read in the decision the user has made (external data or internal PC) and read in user set parameter
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\SD_or_PC.txt`) DO (
SET SD_or_PC=%%F
)

REM Read in the decision the user has made (deleting raw files, yes or no) and read in user set parameter
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\delete_or_keep.txt`) DO (
SET delete_raw=%%F
)

REM Give general warning
echo     --------
echo Notice that the program may crash if you scroll through this console. While the program is running, better leave it alone.
echo     --------

REM This asks the user in the console to enter the folder name of which he wishes the videos to be processed. As used as example before, this could be "171128"
REM and saves it as the variable current_date. This guides any further step to the right directories.
set /p current_date="Enter name of folder to be processed: "

REM If this folder does not exist, close program.
IF NOT EXIST !current_date! EXIT [/B]

REM Move into the right folder.
cd !current_date!



REM If the user selected not to give out- and input folders, create those. First check if there are old ones and delete.
IF !SD_or_PC!==2 (
IF EXIST !main_dir!\get_timestamps\directories\output_folder.txt (
del !main_dir!\get_timestamps\directories\output_folder.txt
)
IF EXIST !main_dir!\get_timestamps\directories\extract_folder.txt (
del !main_dir!\get_timestamps\directories\extract_folder.txt
)
for /d %%a in ("*") do (echo !main_dir!\!current_date!\%%~nxa)>>!main_dir!\get_timestamps\directories\extract_folder.txt
for /d %%a in ("*") do (echo %%~nxa)>>!main_dir!\get_timestamps\directories\output_folder.txt
)

REM If extracting from external source: If a folder from the given list doesn't exist yet, create it.
IF !SD_or_PC!==1 (
for /f "delims=" %%a in (!main_dir!\get_timestamps\directories\output_folder.txt) do (
IF NOT EXIST %%a (
MKDIR %%a
)
)
)


REM Count the number of folders that exist for creating output
set folder_count=0
for /f "delims=" %%a in (!main_dir!\get_timestamps\directories\output_folder.txt) do (
set /a folder_count=!folder_count!+1
)




REM This first big loop covers the tasks until the point where the raw video files were copied to the folder in which the program will strip off sound.
REM In the end, the raw files can get deleted.
REM Running these two loops separately allows to eject all external media before program is finished, if data gets extracted from external memory device.

REM Get loop counter
set extract_counter=1

REM The following commands are executed for each subfolder in the folder selected by the user.
for /L %%x in (1,1,!folder_count!) do (

REM Get current folder name of the loop
set extract_det_counter=1
for /f "delims=" %%a in (!main_dir!\get_timestamps\directories\output_folder.txt) do (
IF !extract_det_counter!==!extract_counter! (
set species=%%a
)
set /a extract_det_counter=!extract_det_counter!+1
)


REM Get current extraction location of the loop
set extract_det_counter=1
for /f "delims=" %%a in (!main_dir!\get_timestamps\directories\extract_folder.txt) do (
IF !extract_det_counter!==!extract_counter! (
set extract=%%a
)
set /a extract_det_counter=!extract_det_counter!+1
)

REM Update loop counter
set /a extract_counter=!extract_counter!+1


REM Access the subfolder
cd !species!

REM This is a safety step: If this script was already run before, this csv file should already exist in the directory. Thus the program is automatically closed.
IF EXIST "results_!current_date!_!species!.csv" EXIT [/B]

REM Pipe all the file names of the first files of each recording unit (one consecutive recording) in a txt file.
(for %%a in (!extract!\GOPR*.mp4) do echo %%~nxa) > starter.txt

REM Call ffmpeg.
REM For each first file of a recording unit, extract all video information and pipe it into a txt file.
for %%a in (!extract!\GOPR*.mp4) do !ffmpeg_dir! -i %%~a > %%~nxa_info 2>&1

REM Extract the line that contains the string "creation_time" and pipe it into a txt file. This contains the date and the time when the recording was started.
for %%i in (*info) do findstr /R [a-z]*creation_time  %%i > forR_%%i_new_info

REM Call R.
REM This script collects all video information together in one txt file. For details, check comments on the R script.
"!R_dir!" CMD BATCH --vanilla --slave "--args !main_dir! !current_date! !species!" !main_dir!\get_timestamps\Extract_video_info1.R

REM Delete those txt files that are not of use anymore.
del forR*

REM strip the audio off from each .mp4 video and save it as a new video under the same name with "a_" in front. 
REM ffmpeg does not offer to overwrite files, therefore it has to be done more complicatedly.
for %%a in (!extract!\*.mp4) do !ffmpeg_dir! -i %%~a -vcodec copy -an a_%%~nxa -hide_banner


REM If wished in setup, delete all raw files inside folder
IF !delete_raw!==1 (
REM Delete all videos with audio and the respective LRV and THM files.
del !extract!\G*.mp4 
del !extract!\G*.LRV 
del !extract!\G*.THM 

REM Copy the two batch scripts that will do the next steps into the folder that we are just in. 
copy !main_dir!\get_timestamps\copy_submit_results_program.bat 000_submit_results.bat
copy !main_dir!\get_timestamps\copy_cut_video_program.bat 000_cut_video.bat
copy !main_dir!\get_timestamps\copy_rerun_opencv_code.bat 000_rerun_opencv_code.bat
copy !main_dir!\get_timestamps\copy_FINAL_submit_delete.bat 000_FINAL_submit_delete.bat

)


REM Move out of the subfolder again
cd ..

)



echo ------------------------
echo ------------------------
echo Extraction of all video files from all given folders completed. If extraction was performed from external devices, they can now be ejected.
echo The program will now merge all video chunks.
echo ------------------------
echo ------------------------




REM Get loop counter
set loop_counter=1


REM The following commands are executed for each subfolder in the folder selected by the user.
for /L %%x in (1,1,!folder_count!) do (


REM Get current folder name of the loop
set detect_counter=1
for /f "delims=" %%a in (!main_dir!\get_timestamps\directories\output_folder.txt) do (
IF !detect_counter!==!loop_counter! (
set species=%%a
)
set /a detect_counter=!detect_counter!+1
)


REM Get current extraction location of the loop
set detect_counter=1
for /f "delims=" %%a in (!main_dir!\get_timestamps\directories\extract_folder.txt) do (
IF !detect_counter!==!loop_counter! (
set extract=%%a
)
set /a detect_counter=!detect_counter!+1
)



REM Access the subfolder
cd !species!


REM Set a variable which will be used for naming the output file. After each iteration of the for-loop, the value of this variable increases by 1.
set counter=1

REM Go through all videos that start a recording unit (=file name begins on a_GOPR). Call those files in the order of their recording time by using starter.txt.
REM Set the video name as variable "str". Overwrite str by only the last 6 characters of str.
REM Pipe all file names ending on str into the file names.txt. Then call ffmpeg and clipp all files from this txt file together. Call the video according to the 
REM variable "counter" (i.e. first video will be called 1.mp4, second will be called 2.mp4).
REM Increase the value of counter by 1.
REM Repeat the same for the next set of files ending on the same last digits as the respective starter of the recording unit. str and names.txt get overwritten.
REM This is a critical step! In order for everything merging well later, we have to make sure that video number 1 is actually the one created first on that
REM day. Using the R output from Extract_video_info1.R, the file starter.txt should ensure this, as this is the list of starter chunks ordered by time of the day.
REM The /f in the for command is necessary to use the file as vector to loop over.

for /f %%i in (starter.txt) do (

set str=%%i
set str=!str:~-6!

(for %%i in (a_*!str!) do @echo file '%%i') > names.txt
!ffmpeg_dir! -f concat -safe 0 -i names.txt -c copy !counter!.mp4
set /a counter=!counter!+1

)

REM Delete those files that are not of use anymore.
del a_*
del names.txt
del starter.txt

REM Echo all file names of the newly created, muted and clipped together videos into a txt file.
REM Those are all mp4 files with no G in the beginning of the file name.
dir * /b | findstr ".mp4" | findstr /v "G" > combined_videos.txt

REM Call ffmpeg to extract all video information from these files.
for /f %%i in (combined_videos.txt) do !ffmpeg_dir! -i %%~i > %%~i_info 2>&1

REM Extract those lines that contain the key word "Duration". This gives the length of each of the videos.
for %%i in (*info) do findstr /R [a-z]*Duration  %%i > forR_%%i_new_info_duration

REM Call R to add the information on video length to the already existing video information table.
"!R_dir!" CMD BATCH --vanilla --slave "--args !main_dir! !current_date! !species!" !main_dir!\get_timestamps\Extract_video_info1_duration.R

REM Delete all files that are not necessary anymore.
del *info
del forR*


echo ------------------------
echo ------------------------
echo Extraction of all video files from all given folders completed. If extraction was performed from external devices, they can be ejected.
echo The program currently runs through video footage of video location number !loop_counter! and produces relevant timestamps. This will take a while.
echo ------------------------
echo ------------------------


REM Call the C++/OpenCV program to output a table which contains all frames that match a certain given condition (either
REM color signal or motion into frame). Do this for each video in the folder. The last entry in the created table is the frame rate.
REM Depending on the decision of the user, either the motion or color detection program will be called.
if !motion_or_color!==1 (
for /f %%u in (combined_videos.txt) do !main_dir!\get_timestamps\MovementIntoFrame.exe !main_dir!\!current_date!\!species!\ %%u !program_parameters! > raw_%%u.txt
)
if !motion_or_color!==2 (
for /f %%u in (combined_videos.txt) do !main_dir!\get_timestamps\CheckMotion.exe !main_dir!\!current_date!\!species!\ %%u !program_parameters! > raw_%%u.txt
)

REM Delete files that are not necessary anymore.
del combined_videos.txt

REM Call R to modify this output in a way that it condenses the information that the C++ program gives down to something more convenient for the user.
REM Check comments in the R script for more.
"!R_dir!" CMD BATCH --vanilla --slave "--args !main_dir! !current_date! !species!" !main_dir!\get_timestamps\Transform_raw_code.R


REM Leave the subfolder.
cd .. 

REM Delete all files that are not needed anymore or are just unnecessary leftovers.
del !main_dir!\get_timestamps\Extract_video_info1.Rout
del !main_dir!\get_timestamps\Extract_video_info1_duration.Rout
del !main_dir!\get_timestamps\Transform_raw_code.Rout


REM Update loop counter
set /a loop_counter=!loop_counter!+1

)



REM END