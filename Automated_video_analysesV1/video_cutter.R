#Written by Alexander Hausmann (alexander_hausmann@gmx.net) 
#in ~ November 2017 for a Windows machine

#This file is executed automatically by the batch script cut_video.bat.

#This R script executes the following tasks (by itself or by calling
#ffmpeg over the command line):
#a) Use timestamps created in data_submission.R to cut out chunks by calling ffmpeg 
#in the command line and Clip all those chunks together into one supercut video.


#Take arguments from command line giving the path information
params<-commandArgs(trailingOnly = TRUE)

#Split the parameter information into three variables.
#From now on, the directory will always be called following these variables.
full_dir<-as.character(params[1])

date<-strsplit(full_dir,"/")[[1]][length(strsplit(full_dir,"/")[[1]])-1]
species<-strsplit(full_dir,"/")[[1]][length(strsplit(full_dir,"/")[[1]])]
main_dir<-paste0(strsplit(full_dir,"/")[[1]][-c(length(strsplit(full_dir,"/")[[1]])-1,
                                                length(strsplit(full_dir,"/")[[1]]))],collapse="/")

#Read out ffmpeg directory
ffmpeg_dir<-as.character(params[2])

#Set working directory to the directory given by the input table. First safe old one.
old_wd<-getwd()
setwd(full_dir)

#Read in the results table of our folder (actually giving the whole directory
#here is not really necessary because we set the working directory; it
#is here just for safety)
day_spec_result<-read.csv(paste0(full_dir,"/results_submit.csv"),
                          header=F)[,c(4,9,15),drop=F]

#Create an empty vector to fill with all file names resulting from all recording units.
filenames<-NULL

#Read in timestamps that were created in the data_submission.R script
timestamps_all<-read.table(paste0(full_dir,"/timestamps.txt"),
                           header=F)

#It may be that two butterflies are in the same frame at a time. 
#Therefore the table would contain two different entries for each individual
#and the same chunk being cut out two times.
#We have to modify the information on the relevant time points thus in a way
#that this does not happen.
#This for loop goes through all the videos (i.e. video-ID 1, 2, etc.)

#Record the names of the new files
names_chunks<-NULL

for(u in unique(day_spec_result[,1])){
  
  timestamps<-timestamps_all[timestamps_all[,1]==u,2:3]
  
  #fps<-read.table(paste0(full_dir,"/raw_",u,".mp4.txt"),
  #                header=F)
  #fps<-fps[,1]
  #fps<-fps[length(fps)]
  
  #This for-loop calls ffmpeg and cuts out the chunks, calculated by
  #the previous for-loop.
  #At the same time, it saves the file name of the resulting chunk 
  #in a format readable to ffmpeg into "filenames".
  for(timer in 1:length(timestamps[,1])){
    #Call ffmpeg to cut out chunk.
    
    #system(paste0(ffmpeg_dir," -ss ",timestamps[timer,1]," -i ",u,".mp4_old -g ",fps," -t ",timestamps[timer,2]," -c copy chunk_",u,"_",timer,".mp4"))
    #system(paste0(ffmpeg_dir," -ss ",timestamps[timer,1]," -i ",u,".mp4_old -force_key_frames expr:gte(t,n_forced*",round(timestamps[timer,2]*fps),") -t ",timestamps[timer,2]," -c copy chunk_",u,"_",timer,".mp4"))
    system(paste0(ffmpeg_dir," -ss ",timestamps[timer,1],".00 -i ",u,".mp4_old -t ",timestamps[timer,2],".00 -c copy chunk_",u,"_",timer,".mp4"))
    #system(paste0(ffmpeg_dir," -ss ",timestamps[timer,1],".00 -t ",timestamps[timer,2],".00 -i ",u,".mp4_old -t ",timestamps[timer,2],".00 -r ",timestamps[timer,2]," -g ",timestamps[timer,2]," -c copy chunk_",u,"_",timer,".mp4"))
    #system(paste0(ffmpeg_dir," -ss ",timestamps[timer,1],".00 -t ",timestamps[timer,2],".00 -i ",u,".mp4_old -vcodec copy -acodec copy chunk_",u,"_",timer,".mp4"))
    
    #Safe filenames together with the necessary extra words and signs for
    #ffmpeg. A line in the table given to ffmpeg needs to look like this:
    #file 'xx'. <File> should not be in quotation marks, but the name of 
    #the file. Double quotation marks though don't work!
    #That's why we put it into "'xx'".
    filenames<-c(filenames,paste("'",paste0("chunk_",u,"_",timer,".mp4"),"'",sep=""))
    names_chunks<-c(names_chunks,paste0("chunk_",u,"_",timer,".mp4"))
  }
  
}

write.table(names_chunks,
            paste0(full_dir,"/names_chunks.txt"),
            quote=F,
            row.names=F,col.names=F)

#Write the filenames into a table without using quotation marks (quote=F).
#This will though keep the '' around the filenames.
write.table(data.frame(rep("file",length(filenames)),filenames),
            paste0(full_dir,"/chunks.txt"),
            quote=F,
            row.names=F,col.names=F)

#Accorind to the filename table, let ffmpeg paste the created chunks together
#again and name them after the video-ID.
system(paste0(ffmpeg_dir," -f concat -safe 0 -i chunks.txt -c copy ",date,"_",species,"_supercut.mp4"))


#Set working directory back to the usual.
setwd(old_wd)


### END
