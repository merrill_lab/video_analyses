
#Take arguments from command line giving the path information
params<-commandArgs(trailingOnly = TRUE)

#Split the parameter information into three variables.
#From now on, the directory will always be called following these variables.
full_dir<-as.character(params[1])

date<-strsplit(full_dir,"/")[[1]][length(strsplit(full_dir,"/")[[1]])-1]
species<-strsplit(full_dir,"/")[[1]][length(strsplit(full_dir,"/")[[1]])]
main_dir<-paste0(strsplit(full_dir,"/")[[1]][-c(length(strsplit(full_dir,"/")[[1]])-1,
                                                length(strsplit(full_dir,"/")[[1]]))],collapse="/")

#Read out ffmpeg directory
ffmpeg_dir<-as.character(params[2])

names_chunks<-unlist(read.table(paste0(main_dir,"/",date,"/",species,"/names_chunks.txt")))
#transform everything to character that is factor
names_chunks<-levels(names_chunks)[names_chunks]

#Read in which chunk contains how many sequences.
which_chunk<-read.table(paste0(main_dir,"/",date,"/",species,"/which_chunk.txt"))[,1]

rec_time<-NULL
for(chunker in names_chunks){
  duration_info<-levels(unlist(read.table(paste0(main_dir,"/",date,"/",species,"/forR_",chunker,"_info.txt"))[2]))
  #Take away the last character, which is a comma
  duration_info<-substr(duration_info,1,nchar(duration_info)-1)
  #Split the time at the colon. hh:mm:ss -> hh mm ss
  duration_info<-strsplit(duration_info,':')
  #Translate this into seconds.
  rec_time<-c(rec_time,as.numeric(duration_info[[1]][1])*3600+as.numeric(duration_info[[1]][2])*60+as.numeric(duration_info[[1]][3]))
}

if(length(rec_time)>1){
  acc_duration<-0
  for(i in 2:length(rec_time)){
    acc_duration<-c(acc_duration,sum(rec_time[1:(i-1)]))
  }
  times_of_behav<-acc_duration[which_chunk]
  times_of_behav_char<-NULL
  for(timser in 1:length(times_of_behav)){
    if(sum(times_of_behav[timser]%in%times_of_behav[-timser])>0){
      times_of_behav_char<-c(times_of_behav_char,paste0(times_of_behav[timser],"(with_others)"))
    } else{
      times_of_behav_char<-c(times_of_behav_char,paste0(times_of_behav[timser]))
    }
  }
  
  old_tab<-read.csv(paste0(full_dir,"/results_submit.csv"),header=F,row.names=NULL)
  #transform everything to character that is factor
  for(i in 1:length(old_tab[1,])){
    if(is.factor(old_tab[,i])){
      old_tab[,i]<-levels(old_tab[,i])[old_tab[,i]]
    }
  }
  old_tab[,10]<-times_of_behav_char
  write.table(old_tab,paste0(full_dir,"/results_submit.csv"),
              row.names=F,col.names=F,sep=",")
}
