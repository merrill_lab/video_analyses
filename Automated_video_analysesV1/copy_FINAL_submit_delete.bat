@echo off
REM Let Windows not echo anything unless it is asked to do so

REM Written by Alexander Hausmann (alexander_hausmann@gmx.net) in ~ April 2018 for a Windows machine

REM This batch file can be executed either by double-clicking on the file name or calling it from the console.

REM This batch script executes the following tasks:
a) Submitting the data to the final csv.
b) Deleting leftover files.
c) Announcing the folder as finished.

REM This is a safety step: If results were not yet submitted to the main results file, submit_results.bat should still exist in the directory. 
REM Thus the program will be automatically closed.
IF NOT EXIST "results_submit.csv" EXIT [/B]
IF NOT EXIST *supercut.mp4 EXIT [/B]

REM Remember current directory
set current_dir=%cd%

for %%F in ("%cd%") do set "only_folder_species=%%~nxF"

REM Go down two levels. Safe folder name in between.
cd..
for %%F in ("%cd%") do set "only_folder_date=%%~nxF"
cd..

REM Update main directory file
cd > get_timestamps\directories\main_dir.txt

REM Read in directories and save them on variables.
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\main_dir.txt`) DO (
SET main_dir=%%F
)
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\results_name.txt`) DO (
SET results_name=%%F
)

REM Go back to subfolder
cd %current_dir%

REM Delete full duration video files.
del *_old

REM Append information from this new csv file to the main results csv in the mother folder.
copy /b %main_dir%\%results_name%.csv + results_submit.csv %main_dir%\%results_name%.csv

REM Safe folder as done.
echo %only_folder_date% >> %main_dir%\done_%only_folder_species%.txt

REM Delete 000_submit_results.bat and this file.
del 000_submit_results.bat
del 000_FINAL_submit_delete.bat

REM END
