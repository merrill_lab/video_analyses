CheckMotion is an OpenCV program written in C++. It was written to detect butterflies in a frame by identifying the blue acrylic 
paint on the thorax of the butterflies. We have deccided to use a different approach to detect butterflies but still save the codes and 
instructions here for any future reference and use.

## List of folders ##

1. The folder "CheckMotion": contains 3 OpenCV C++ codes, CheckMotion_Clean_and_Final.cpp, CheckMotion.cpp and 
CheckMotion_OldVersion.cpp. As the names indicate, the latter is an older version of the 
second. Version one is the clean and a little more efficient version which is also called by the batch scripts.
The differences between the 2 versions are minor, mostly in what the outputs are and the ways the outputs are presented.

2. The folder “CheckMotion.xcodeproj”: contains everything required to run CheckMotion in Xcode.

## Other instructions ##

Detailed instructions on how to install, configure, and run OpenCV on Windows and Mac can be found in the document "How_to_use_CheckMotion.md"
The comments in the files CheckMotion_Clean_and_Final.cpp and CheckMotion.cpp have more information on customizing outputs, etc.