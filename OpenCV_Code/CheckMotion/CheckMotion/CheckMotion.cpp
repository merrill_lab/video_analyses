//opencv
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include <fstream>
//C
#include <stdio.h>
//C++
#include <iostream>
#include <sstream>
#include <tbb/blocked_range.h>
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>
#include <tbb/task_scheduler_init.h>
#include <tbb/mutex.h>
#include <tbb/tbb_thread.h>
#include <tbb/blocked_range2d.h>
#include <dirent.h>
#include <sys/stat.h>

using namespace cv;
using namespace std;
using namespace tbb;



//    Note for program CheckMotion:
//
//    CheckMotion does the following
//    1) It searches each frame in a video and looks for a specified range of colors defined by BGR codes
//    2) Pixels falling within the range will be converted to white while everything else converted to black
//    3) Outputing the frame number for frames that contain the target colors and frame rate (fps) of the video
//    4) Optional outputs:
//        A. Display each frame as it is read into the program
//        B. Png files of the orignal and thresholded (i.e. black and white) images for frames with target colors    
//        C. Number of threads and program run time
//        D. Number of total pixels and number of pixels for target colors
//       Uncomment the corresponding codes marked with the alphabets to enable the optinoal outputs


int64 startTime;

int NumThreads = task_scheduler_init::default_num_threads();

int main(int argc, char* argv[])
//3 arguments: argv[1]: filming date (e.g. 171117); argv[2]: species (MP or CP); argv[3]: filename (e.g. 1.mp4)
{
    ///Part I: Read-in the video
    // First part of the directory containing the video files. Change it per user's preference 
    char folder[50] = "/Users/chi/Desktop/Video analyses/test videos/";
    char* species;
    
    if (strcmp(argv[2],"MP") == 0) {
        species = "/Melpomene/";
    }
    if (strcmp(argv[2],"CP") == 0) {
        species = "/Cydno/";
    }
    
    //A series of concatenation to include user inputs for filming date and species as the location for video files
    char* filelocation1 = strcat(folder,argv[1]);
    char* filelocation2 = strcat(filelocation1,species);
    char* filelocation = strcat(filelocation2,argv[3]);
    
    //Input video
    VideoCapture cap(filelocation);
    
    //Create an variable (integer plus decimals) to store frame rate information
    double fps;
    fps = cap.get(CAP_PROP_FPS);
    
    if(cap.isOpened()) {
        
        //Getting start time info for calculating total run time
        startTime = getTickCount();
        
        //Define a stringstream for storing frame numbers for those frames with butterflies
        //Has to be outside of the for loop so it gets updated
        stringstream butterfly;
        
        //Create an object denoting the frames
        Mat frame;
        
        //The for loop that looks through frames
        for(;;)
        {
            //The two lines below are just for calculating program run time
            double tfreq = getTickFrequency();
            double secs = ((double) getTickCount()-startTime)/tfreq;
            
            cap >> frame;
            
// ***** Optional Output A: Display video frames as they are read into the program *****

            // Uncomment the codes below to activate this functionality 
            //            namedWindow("Frame");
            //            imshow("Frame",frame);
            //            waitKey(5);
            
//**************************************************************************************            
            
            //Create a string for frame number that gets updated for each cycle of the loop
            stringstream ss;
            
            //Retrieving frame number info from VideoCapture
            ss << cap.get(CAP_PROP_POS_FRAMES);

// ***** Optional Output B: Exporting original and thresholded frames as image files *****
            
            // Uncomment the codes below to activate this functionality 
            // 
            //         string FrameNumberString = ss.str();
            //         stringstream maskedfilename;
            //         stringstream rawfilename;
            //         maskedfilename << "/Users/chi/Desktop/test/masked" << FrameNumberString.c_str() << ".png";
            //         rawfilename << "/Users/chi/Desktop/test/raw" << FrameNumberString.c_str() << ".png";
            
//**************************************************************************************
            
            ///Part II: Image thresholding and image saving
            
            //Create an object representing new images after thresholding
            Mat masked;
            
            //inRange function that convert the pixels that fall within the specified range to white and everything else to black
            //The Range is specified by a lower [Scalar(10,0,90)] and an upper [Scalar(50,50,170)] threshold
            //In openCV, a color is defined by its BGR score
            //The thresholded images will then be represented by the object "masked"
            inRange(frame, Scalar(0,0,0), Scalar(100,100,100), masked);
            
            //Creating integer variables for total pixel count and white pixel count for each frame
            int totalpixel;
            int whitepixel;
            
            //Total pixel count equals the number of rows and columns of the frame
            totalpixel = frame.rows*frame.cols;
            //Using countNonZero function to count the number of white pixels
            whitepixel = countNonZero(masked);
            
            //Exit the loop when reaching the last frame (i.e. pixel count drops to 0)
            if(totalpixel==0){
                cap.release();
                cout
                << fps << '\n'

// ***** Optional Output C: Outputing program run time and number of threads *****
            

                //<< "Run time = " << fixed << secs <<" "<< "seconds" << '\n'
                //<< "Number of threads: " << NumThreads << '\n'
                
//**************************************************************************************
                
                << '\n';
                break;
            }
            
            else {
                if (whitepixel > 0){
                    cout
                    << ss.str() << '\n';
                    
// ***** Optional Output D: Outputing number of total pixels and number of pixels of target colors *****

                    // << "Number of total pixels:" << totalpixel << '\n'
                    // << "Pixels of target colors:" << whitepixel << '\n'
                    // << '\n';
          
//**************************************************************************************

                    // Below are the codes for exporting a csv file containing number of frames with target colors
                    // First, open a csv file at the designated place
                    ofstream ofs("/Users/chi/Desktop/Video analyses/butterfly.csv",ofstream::out);
                    // Second, adding new frame numbers to the string "butterfly" based on the whitepixel condition set above
                    // The '/n' here is critical since it moves every new entry to a new line, thereby creating a column of frame numbers
                    butterfly << ss.str() << '\n';
                    // Third, update the csv as butterfly gets updated
                    ofs << butterfly.str();
                    // Fourth, close the csv
                    ofs.close();

// ***** Optional Output B: Exporting original and thresholded frames as image files *****

                    // Export original and thresholded images
                    // imwrite(rawfilename.str(),frame);
                    // imwrite(maskedfilename.str(),masked);

//**************************************************************************************

                }
            }
        }
    }
    exit(0);
}
