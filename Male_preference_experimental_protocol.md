## General protocol for male preference assays ##

Chi-Yun Kuo and Alexander Hausmann (with additional comments by Merrill), December 2017

## Butterfly housing and husbandry ##
After eclosion, we will give each male 2-3 hours to allow their wings to harden. We will then give each male a unique mark (see Marking section) before placing it in the experimental cage (legnth x width x height = 4.5 m x 3.1 m x 2.6 m). We use freshly eclosed males to avoid confounding effects of prior mating experience on mate preference. 

The cage has several *Psychotria poeppigiana* plants on which the butterflies can feed. We will also provide fresh flowers of *Psiguria* and *Lantana* daily and sugar water every other day as additional food sources. Preference assays are performed whenever possible to maximise data for individual males regardless of the number of males. However, the number and identity of males within the choice cage is tracked and recorded daily. 

A male will be removed from the experiment under one of the 2 conditions: 1. after X days or 2. when we have enough data to confidently phenotype its mate preference (see Data analyses) , or 3. when individuals look sickly, so that they can be preserved before they die. We will introduce males to the experimental cage as they become available.

## Marking ##
At the time of eclosion, each male will be given a unique number, written with a water-insoluble black marker pen (*brand and city*) on the underside of the left forewing. This number (ID_insectary) registers the individual in the bookkeeping system of the whole insectary and is independent of the experimental ID (ID_exp) we will assign to the same individual for the experiment. 

Each male will also be uniquely marked by writing small crosses within the red or white color bands on the dorsal surface of the forewings. On each forewing there can be 0-4 crosses, allowing for a total of 25 possible combinations for each species for individual identification. The individual ID information will be entered in the database "individual_information.csv", which contains the following columns:



* ID_experiment (e.g. 15)

* ID_insectary (e.g. B17-123)

* Species or hybrid type (MP for Melpomene and CP for Cydno. F1=MPCP or CPMP. Backcrosses e.g. MP(MPCP)

* Marking: in the format of a_b; a and b are numbers of Xs painted on the left and right forewings, respectively, and can be integers from 0 to 4.

* Date born (year_month_day in numbers and with UNDERSCORE)

* Date entering cage (year_month_day in numbers and with UNDERSCORE)

* Date exiting cage (year_month_day in numbers and with UNDERSCORE)

* DNA identification number (e.g. CAM123456)

* Note: reason for exiting the cage (unexpected death, poor body condition, completion of data collection, escaped, missing, etc), 

After marking, we will photograph the dorsal view of the butterfly to assist in individual identification from the videos and safe it with the insectary_ID as filename.

## Daily butterfly bookkeeping

Every day before setting up mate preference trials, we will survey the butterflies in the cage. Any mortality or missing individuals will be recorded on the Daily Spreadsheet.  

In the afternoon after the trials, we will add new individuals to the experimental cage if necessary. The information for the added individuals (Species, ID_insectary, and Marking) will be recorded on the Daily Spreadsheet and updated on the Daily Spreadsheet on the same day.


## Butterfly model construction ##
Use diagrams/pictures to illustrate how to make a butterfly model and how to build the device that vibrate the model during mate preference trials.

## Mate preference experiments ##
Mate preference experiments will be performed and filmed as 4 one-hour trials between 9:00 am to 2:00 pm. In each trial we will present the butterflies with one model of each test species (hereafter species A and B). The locations of the models in the cage will change between trials based on the following procedure:

Before the first trial, we randomly decide the locations of the two models from 6 possible locations in the cage (the two models cannot occupy the same location). The locations of the models will remain the same within the trial. Before the beginning of the next trial, we will randomly assign the locations of one model to one of the five possible locations that have not been occupied by this model. We will then do the same with the other model. The same procedure occurs at the beginning of the two subsequent trails, which ensures that the models will never appear at the same locations between trials performed on the same day. Given Heliconius butterflies' capacities for spatial learning, this procedure may be important in presenting the models as new stimuli by changing the association between models and their locations in the cage between trials.

We place a camera (GoPro Hero 5 Black) about 50 cm above each model and record continuously for an hour with the following settings: 

* Resolution: 1080P

* Filming mode: Narrow

* Frame rate: 60 fps

* Max ISO: 1600 (with this setting GoPro automatically adjusts ISO from 400 to 1600 based on ambient lighting)

* Shutter speed: 1/480 sec

The pairing between camera and model will also be randomly decided daily before the first trial starts. In addition, we will record the light environment of each model with a lumen logger (*fill in brand and city*) sampling at a frequency of 1/sec. The loggers will move with the models between trials.

We will also record the filming start time and temperature (*assuming we don't get the logger for it*) on the Daily Speadsheet for each trial.

## Video analyses ##

We start a lab session with analyzing the results of the previous day. We open the following files:
- Video file (1, 2, 3, or 4)
- TIMESTAMPS text file showing instances of movement
- Visualization of what is in the experimental cage
- Daily visual schedule
- Daily spreadsheet for data entry

We go through the videos and enter the data into the daily spreadsheet (see below, Data Entry). 
We then click the submit_results batch script, which will unite daily spreadsheet with information on video recordings and concatenate it to the general results sheet.
We then click the cut_video batch file that produces a video supercut according to the given data.

We then update the individual database and click GET_VISUAL_IDs to produce an up-to-date overview over what is currently in the cage and to more easily identify individuals. We safe that overview on our smartphone/tablet.

We will finally connect the two SD cards to the lab computer and get the timestamps for the videos produced on this day by clicking on GET_TIMESTAMPS.bat. The Readme file in the folder "Automated_video_analyses" has detailed instructions on how to extract footages with butterflies from raw videos. The batch program will produce a Table containing time stamp for each captured appreance of butterflies. We will then count the number of frames over which a butterfly is in close proximity to a model and convert the number of frames to time (minimum unit = 1/60 sec). 
We use the program under the settings we gave in the program's setup. We test for motion into frame at a sensitivity of 100 and a blur level of 10. *Currently we don't exclude an area around the model as the model doesn't move. This though can be done by deciding so in the setup.*

## Data entry ##

At detection of a butterfly showing attraction behaviour to a model, we will enter daily raw data into a spreadsheet with the following columns:

* Observer (any character string)

* Camera_ID (capitol letters, e.g. A and B)

* Camera_Position (1-6 in cage)

* Model (MP or CP)

* Model_ID (e.g. 1)

* Video_ID (for four recording units per day, this can range between 1 and 4)

* ID_insectary

* Start time of behaviour in video (in seconds)

* Duration of frames that model spent in frame

* Chasing with other individuals during attraction (write ID of other individual and in front of it A (active), P (passive) or N (neutral, unclear))

* Comments

## Data analyses ##

### Phenotyping mate preference ###

For each male, we will plot percent time associated with the A (or B) model [i.e. preference for A(P_A), calculated as time_A/(time_A + time_B)] against total time recorded (time_total = time_A + time_B) cumulatively. As time_total increases, P_A should level off. A plateau would indicate that we've captured the stable preference of that male. Time_total needed to reach the plateau might also be another interesting metric for mate preference. However, we will also remove a male from the experiment if it exhibits low interest in both models after 15 days (*number of days is tentative*).

### Preliminary statistical model ###

Assuming we will use P_A as a proxy for mate preference, the statistical model would be:

(Will need to read up on QTL analyses [and we can probably keeep those analysis details elsewhere - they will very much depend on the data])

P_A ~ marker 1 + marker 2 + ... , with and without age as a covariate. Use model selection (most likely AIC) to see if including age as a covariate improves the model.

