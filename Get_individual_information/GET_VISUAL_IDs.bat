@ECHO OFF

REM Set current directory
set current_dir=%cd%

REM Give name of the folder
for %%* in (.) do set current_folder=%%~nx*

cd ..

REM Read in directories and save them on variables.
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\main_dir.txt`) DO (
SET main_dir=%%F
)
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\R_dir.txt`) DO (
SET R_dir=%%F
)
FOR /F "tokens=* USEBACKQ" %%F IN (`more get_timestamps\directories\results_name.txt`) DO (
SET results_name=%%F
)

REM Go back into folder
cd %current_dir%

set main_dir=%main_dir:\=/%

REM Execute R
"%R_dir%" CMD BATCH --vanilla --slave "--args %main_dir% %results_name% %current_folder%" %current_dir%\Visualize_IDs.R

del Visualize_IDs.Rout

REM END